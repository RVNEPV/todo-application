<?php
class API{
    function name($name){
        echo "Full name: {$name}\n<br>";
    }
    function hobbies($hobbies){
        echo "Hobbies: \n<br>";
        foreach ($hobbies as $hobby) {
            echo " {$hobby}\n<br>";
        }
    }
    function details ($age, $email, $birthday){
        echo "Age: {$age}\n<br>Email: {$email}\n<br>Birthday: {$birthday}";
    }
}
   $API = new API();
   $API->name('Ronerr Villacarlos');
   $API->hobbies(["Playing", "Watching Movies"]);
   $API->details('22', 'ronerr.villacarlos@gmail.com', 'May 14, 2002');
?>
