<?php

/**
 * Tells the browser to allow code from any origin to access
 */
header("Access-Control-Allow-Origin: *");

/**
 * Tells browsers whether to expose the response to the frontend JavaScript code
 * when the request's credentials mode (Request.credentials) is include
 */
header("Access-Control-Allow-Credentials: true");

/**
 * Specifies one or more methods allowed when accessing a resource in response to a preflight request
 */
header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");

/**
 * Used in response to a preflight request which includes the Access-Control-Request-Headers to
 * indicate which HTTP headers can be used during the actual request
 */
header("Access-Control-Allow-Headers: Content-Type");

require_once('MysqliDb.php');



class API {
    private $db;
    public function __construct()
    {
        // Initialize MySQLiDB connection
        $this->db = new MysqliDb('localhost', 'ronerr', '', 'ronerr'); //localhost, username, password, database
    }

    /**
     * HTTP GET Request
     *
     * @param $payload
     */
    public function httpGet($payload)
    {
        //If looking for ID 
        if (isset($payload['id']) && !empty($payload['id'])) {
            //Code to fetch ID 
            $id = $payload['id'];
            $this->db->where('id', $id);
            $employee = $this->db->getOne('employee');

        // Check if query was successful
        if ($employee) {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $employee,
            ));
        } else {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'fail',
                'message' => 'Task not found'
            ));
        }
    } else {
        // Give all details in table if no ID 
            $employee = $this->db->get('employee');

        // Check if query was successful
        if ($employee) {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'success',
                'data' => $employee,
            ));
        } else {
            echo json_encode(array(
                'method' => 'GET',
                'status' => 'fail',
                'message' => 'Failed Fetch Request'
            ));
        }
    }
}

    /**
     * HTTP POST Request
     *
     * @param $payload
     */
    public function httpPost($payload)
    {
        // Your Code starts here
        //Will check if the payload is an array and if it is not empty. 
        if (!is_array($payload) || empty($payload)){
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'fail',
                'message' => 'Error Request: Invalid or empty payload'
            ));
            return;
        }
        //Will execute a query using the MysqliDB class to insert data into the database, 
        $data= array(
            'task_title' => $payload['task_title'],
            'task_name' => $payload['task_name'],
            'time' => $payload['time'],
            'status' => $payload['status']
        );

        $id = $this->db->insert('employee', $data);
        

        // Check if insertion was successful
        if ($id) {
        // Fetch the inserted record
        $this->db->where('id', $id);
        $insertedRecord = $this->db->getOne('employee');

        echo json_encode(array(
            'method' => 'POST',
            'status' => 'success',
            'data' => $insertedRecord // Return the inserted record details
        ));
        } else {
            echo json_encode(array(
                'method' => 'POST',
                'status' => 'fail',
                'message' => 'Failed to Insert Data'
            ));
        }
    }

    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     */
    public function httpPut($id, $payload)
    {
        //Will check if $payload is not empty
        if (!is_array($payload) || empty($payload)) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'fail',
                'message' => 'Error Request: Invalid or empty payload'
            ));
            return;
        }

        // Check if ID is not null or empty
        if (is_null($id) || empty($id)) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'fail',
                'message' => 'Error Request: ID is missing or empty'
            ));
            return;
        }

        // Extract the ID from the payload
        $id = $payload['id'];

        // Match the two ids 
        if ($id != $payload['id']) {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'fail',
                'message' => 'Error Request: ID mismatch'
            ));
            return;
        }

        // Update
        $data = array(
            'task_title' => $payload['task_title'],
            'task_name' => $payload['task_name'],
            'time' => $payload['time'],
            'status' => $payload['status']
        );

        //Use where() function
        $this->db->where('id', $id);
        if ($this->db->update('employee', $data)) {
            // Fetch the updated record
            $this->db->where('id', $id);
            $updatedRecord = $this->db->getOne('employee');
    
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'success',
                'data' => $updatedRecord // Return the updated record details
            ));
        } else {
            echo json_encode(array(
                'method' => 'PUT',
                'status' => 'fail',
                'message' => 'Failed to Update Data'
            ));
        }
    }

    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     */
    public function httpDelete($id, $payload)
    {
        // Check if ID is not null or empty
        if (is_null($id) || empty($id)) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'fail',
                'message' => 'Error Request: ID is missing or empty'
            ));
            exit;
        }

        // Check if payload is not empty
        if (empty($payload)) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'fail',
                'message' => 'Error Request: Payload is empty'
            ));
            exit;
        }

        // Check to match
        if (!is_array($payload) && $id != $payload) {
            echo json_encode(array(
                'method' => 'DELETE',
                'status' => 'fail',
                'message' => 'Error Request: ID mismatch'
            ));
            exit;
        }

  // Fetch the record to be deleted
  $this->db->where('id', $id);
  $recordToDelete = $this->db->getOne('employee');

  if (!$recordToDelete) {
      echo json_encode(array(
          'method' => 'DELETE',
          'status' => 'fail',
          'message' => 'Record not found'
      ));
      exit;
  }

  // If the payload is not an array, delete a single record
  if (!is_array($payload)) {
      $this->db->where('id', $id);
      if ($this->db->delete('employee')) {
          echo json_encode(array(
              'method' => 'DELETE',
              'status' => 'Deleted Successfully',
              'data' => $recordToDelete // Return the details of the deleted record
          ));
      } else {
          echo json_encode(array(
              'method' => 'DELETE',
              'status' => 'fail',
              'message' => 'Failed to Delete Data'
          ));
      }
  } else {
      // If the payload is an array, delete multiple records using SQL IN operator
      $this->db->where('id', $payload, 'IN');
      if ($this->db->delete('employee')) {
          echo json_encode(array(
              'method' => 'DELETE',
              'status' => 'Deleted Successfully',
              'data' => $recordToDelete // Return the details of the deleted record
          ));
      } else {
          echo json_encode(array(
              'method' => 'DELETE',
              'status' => 'fail',
              'message' => 'Failed to Delete Data'
          ));
      }
  }
  exit;
    }
}
//Identifier if what type of request
$request_method = $_SERVER['REQUEST_METHOD'];

// Payload data
if ($request_method === 'GET') {
    $received_data = $_GET;
} else {
    //check if method is PUT or DELETE, and get the ids on URL
    if ($request_method === 'PUT' || $request_method === 'DELETE') {
        $request_uri = $_SERVER['REQUEST_URI'];
        $ids = null;
        $exploded_request_uri = array_values(explode("/", $request_uri));
        $last_index = count($exploded_request_uri) - 1;
        $ids = $exploded_request_uri[$last_index];
    }
    //payload data
    $received_data = json_decode(file_get_contents('php://input'), true);
}

$api = new API;

//Checking if what type of request and designating to specific functions
switch ($request_method) {
    case 'GET':
        $api->httpGet($received_data);
        break;
    case 'POST':
        $api->httpPost($received_data);
        break;
    case 'PUT':
        $api->httpPut($ids, $received_data);
        break;
    case 'DELETE':
        $api->httpDelete($ids, $received_data);
        break;
}
?>
