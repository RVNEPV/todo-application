<?php 
require_once($_SERVER['DOCUMENT_ROOT'].'/todo-application/vendor/thingengineer/mysqli-database-class/MysqliDb.php');


function init(){
    $host = 'localhost';
    $username = 'ronerr';
    $password = '';
    $database = 'ronerr';


$conn = new mysqli($host, $username, $password, $database);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
else 
    echo "Connection Created<br>";
    return new MysqliDb($conn);
}

$db = init();

function deleteUser($id){
    global $db;
    $db->where('id', $id);
    if ($db->delete('users')) {
        echo 'Successfully deleted';
    } else {
        echo 'Error: ' . $db->getLastError();
    }
}

function updateUser($id, $first_name, $last_name, $middle_name, $address) {
    global $db;

    $data = [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'middle_name' => $middle_name,
        'address' => $address
    ];
    $db->where('id', $id);
    if ($db->update('users', $data)) {
        echo "<script>alert('User Updated Successfully');</script>";
    } else {
        echo "Error: " . $db->getLastError();
    }
}
?>