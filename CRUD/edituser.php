<?php
require 'functions.php';

init();
$db = MysqliDb::getInstance();

if (isset($_GET['id'])) {
    $id = intval($_GET['id']);
    $db->where('id', $id);
    $user = $db->getOne('users');
    if (!$user) {
        echo "User not found.";
        exit();
    }
} else {
    echo "No user ID provided.";
    exit();
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $middle_name = $_POST["middle_name"];
    $address = $_POST["address"];
    updateUser($id, $first_name, $last_name, $middle_name, $address);
    header("Location: index.php");
    exit();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit User</title>
</head>
<body>
    <h1>Edit User</h1>
    <form method="POST" action="">
        <label>First Name:</label><br>
        <input type="text" name="first_name" required value="<?php echo htmlspecialchars($user['first_name']); ?>"><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="last_name" required value="<?php echo htmlspecialchars($user['last_name']); ?>"><br><br>
        <label>Middle Name:</label><br>
        <input type="text" name="middle_name" required value="<?php echo htmlspecialchars($user['middle_name']); ?>"><br><br>
        <label>Address:</label><br>
        <input type="text" name="address" required value="<?php echo htmlspecialchars($user['address']); ?>"><br><br>
        <button type="submit" name="submit">Update</button>
    </form>
</body>
</html>
