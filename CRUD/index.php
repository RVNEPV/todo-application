<?php
require ('functions.php');

init();
$db = MysqliDb::getInstance();

if (isset($_POST["submit"])) {
    $first_name = $_POST["first_name"];
    $last_name = $_POST["last_name"];
    $middle_name = $_POST["middle_name"];
    $address = $_POST["address"];

    
    $data = Array ("first_name" => $first_name,
    "middle_name" => $middle_name,
    "last_name" => $last_name,
    'address'=>  $address,
    );
    $id = $db->insert ('users', $data);
    if($id){
            echo 'User created with Id= ' . $id;
    } else {
        echo "Error: " . $conn->error;
    }
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST["delete"])) {
        $id = intval($_POST["id"]);
        deleteUser($id);
    }
}
$users = $db->get('users');
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIMPLE CRUD</title>
</head>
<body>
    <h1>CRUD TEST</h1>
    <form method="POST" action="">
        <label>First Name:</label><br>
        <input type="text" name="first_name" required><br><br>
        <label>Last Name:</label><br>
        <input type="text" name="last_name" required><br><br>
        <label>Middle Name:</label><br>
        <input type="text" name="middle_name" required><br><br>
        <label>Address:</label><br>
        <input type="text" name="address" required><br><br>
        <button type="submit" name="submit">Submit</button>
    </form>

    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle Name</th>
                <th>Address</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
                <tr>
                    <td><?php echo $user['id']; ?></td>
                    <td><?php echo $user['first_name']; ?></td>
                    <td><?php echo $user['last_name']; ?></td>
                    <td><?php echo $user['middle_name']; ?></td>
                    <td><?php echo $user['address']; ?></td>
                    <td>
                        <form method='GET' action='edituser.php' style='display:inline-block;'>
                            <input type='hidden' name='id' value='<?php echo $user['id']; ?>'>
                            <button type='submit'>Edit</button>
                        </form>
                        <form method='POST' style='display:inline-block;'>
                            <input type='hidden' name='id' value='<?php echo $user['id']; ?>'>
                            <button type='submit' name='delete' onclick='return confirm("Are you sure you want to delete this record?");'>Delete</button>
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</body>
</html>
