<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{
    public function iShouldGet(ApiTester $I)
    {
    $I->sendGET('/todo-application/to-dolist/API.php');
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseContainsJson(['status' => 'success']);
    }
    
    public function iShouldPost(ApiTester $I)
    {
    $I->haveHttpHeader('Content-Type', 'application/json');
    $I->sendPOST('/todo-application/to-dolist/API.php/', [
        'task_title' => 'Updated Task Title',
        'task_name' => 'Updated Task Name',
        'time' => '2024-06-27 10:00:00',
        'status' => 'Done'           
    ]);
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldPut(ApiTester $I)
    {
    $I->haveHttpHeader('Content-Type', 'application/json');
    $I->sendPUT('/todo-application/to-dolist/API.php/5', [ //Change the number from 5 to any id to Update
        'id' => 5,
        'task_title' => 'Dont Task Me',
        'task_name' => 'Tasked With me ',
        'time' => '2024-06-27 12:21:12',
        'status' => 'Done'
    ]);
    $I->seeResponseCodeIs(200);
    $I->seeResponseIsJson();
    $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldDelete(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        // Replace with the ID of the record to delete
        $I->sendDELETE('/todo-application/to-dolist/API.php/20',
        [20]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }
    // tests

}




//Creating First API test

/*class TodoListCest
{
    public function _before(ApiTester $I)
    {
    }
    // tests
    public function tryToTest(ApiTester $I)
    {
    }
}


*/